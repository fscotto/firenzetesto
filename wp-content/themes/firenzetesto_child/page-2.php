<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width" charset="uft-8" />
    <title>Testo - Fiera del libro Firenze</title>
    <meta name="description" content="Testo &egrave; un&grave;esposizione, una fiera, un evento con al centro un&grave;attenzione sistematica per il mondo del libro. Si svolge dal 20 al 22 Marzo 2019 a Firenze presso la Stazione Leopolda.">
    <link rel="icon" type="image/png" href="img/favicon.png" />
    <link href="/wp-content/themes/firenzetesto_child/landing/css/style.css" rel="stylesheet" type="text/css" media="screen" />   <!-- Mio Style -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<body>
<div id="c-img-sfondo">

    <!-- barra nera -->
    <!-- <div class="c-black-bar c-hide-on-mobile"></div> -->

    <div class="container c-padding-top-container">
        <div class="row justify-content-center">
            <div class="col-sm-8 d-none d-sm-block">
                <img src="img/animazione.gif" class="c-width-100" />
            </div>
            <div class="col-8 d-block d-sm-none">
                <img src="img/animazione-orizzontale.gif" class="c-width-100 c-margin-top-50" />
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-8 c-margin-top-40">
                <p class="c-text-black font-weight-bold">__ Per informazioni scrivi a</p>
                <p class="c-text-black font-weight-bold c-padding-left-5perc"><a href="mailto:info@firenzetesto.com" class="c-text-black font-weight-bold">info@firenzetesto.com</a></p>
                <p class="c-text-black font-weight-bold">__ Per partecipare scrivi a</p>
                <p class="c-text-black font-weight-bold c-padding-left-5perc"><a href="mailto:commerciale@firenzetesto.com" class="c-text-black font-weight-bold">commerciale@firenzetesto.com</a></p>
                <p class="c-text-black font-weight-bold">__ Scarica il</p>
                <p class="c-text-black font-weight-bold c-padding-left-5perc"><a href="Comunicato_stampa_TESTO.pdf" download="Comunicato stampa presentazione TESTO_20-22 marzo" class="c-text-black font-weight-bold">Comunicato stampa</a></p>
                <!-- <p class="c-text-black font-weight-bold">__ Per rimanere aggiornato su tutte le<br><span class="c-padding-left-5perc">novit&agrave; dell'evento</span></p> -->
                <!-- AGGIUNGERE form + checkbox privacy-->
            </div>
        </div>

        <div class="row c-margin-top-footer">
            <div class="col-12 c-padding-top-bottom-5">
                <p class="copyright">Stazione Leopolda srl, Via Faenza 113, 50123 Firenze. P.IVA e C.F. 02339950483, Mail: <a href="mailto:info@stazione-leopolda.com">info@stazione-leopolda.com</a></p>
            </div>
        </div>
    </div>


</div> <!-- /chiusura immagine di sfondo -->
</body>
</html>