    <?php
    /**
     * The template for displaying the footer
     *
     * Contains the opening of the #site-footer div and all content after.
     *
     * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
     *
     * @package WordPress
     * @subpackage Twenty_Twenty
     * @since 1.0.0
     */
    
    ?>
    <footer id="site-footer" role="contentinfo" class="header-footer-group">
    
        <div class="section-inner">
            <div class="vc_row wpb_row vc_row-fluid cont-footer">
            	<div class="wpb_column vc_column_container vc_col-xs-12 vc_col-md-3 single-footer-1">
            		<div class="vc_column-inner">
            			<div class="wpb_wrapper">
    
                            <div class="footer-logo">
                                <img src="/wp-content/themes/firenzetesto_child/img/Logo-50-BlackBG-01.png" alt="Firenze TESTO" title="Firenze TESTO">
                                
                            </div>
    
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container vc_col-xs-12 vc_col-md-2 single-footer-2">
                	<div class="vc_column-inner">
                		<div class="wpb_wrapper">
                            <?php
                            if ( is_active_sidebar('bf-2') ) : ?>
                                <div class="footer-widget-area" role="complementary">
                                    <?php dynamic_sidebar( 'bf-2' ); ?>
                                </div>
                            <?php endif; ?>
    
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container vc_col-xs-12 vc_col-md-2">
                	<div class="vc_column-inner">
                		<div class="wpb_wrapper">
    
    
                            <?php
                            if ( is_active_sidebar('bf-3') ) : ?>
                                <div class="footer-widget-area" role="complementary">
                                    <?php dynamic_sidebar( 'bf-3' ); ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container vc_col-xs-12 vc_col-md-5 single-footer-4">
                	<div class="vc_column-inner">
                		<div class="wpb_wrapper">
                            <div class="footer-credits">
    
                                <div class="footer-link">
                                    <a social target="_blank" href="https://www.facebook.com/stazioneleopoldafirenze"><img width="25px" height="25px" src="/wp-content/themes/firenzetesto_child/img/footer/facebook.png" alt="facebook"/></a>
                                    <a social target="_blank" href="https://www.instagram.com/stazione_leopolda/"><img width="25px" height="25px" src="/wp-content/themes/firenzetesto_child/img/footer/instagram.png" alt="instagram"/></a>
				    <br/>
				</div>
				<div class="copyright-testo">
                                    <p class="powered-by-testo">
                                        <?php _e( '© 2020 Testo.' , 'twentytwenty' ); ?> | 	<a href="<?php echo esc_url( home_url( '/cookie-policy' ) ); ?>"><?php _e( 'Cookie Policy', 'twentytwenty' ); ?></a>
                                       <!-- <a href="<?php echo esc_url( home_url( '/termini_condizioni' ) ); ?>"><?php _e( 'Termini e condizioni', 'twentytwenty' ); ?></a>-->
                                    </p><!-- .powered-by-wordpress -->

                                </div>
                            </div><!-- .footer-credits -->
                        </div>
                    </div>
                </div>
            </div>
    
    
    
        </div><!-- .section-inner -->
    
    </footer><!-- #site-footer -->
    <?php wp_footer(); ?>
    
    </body>
    </html>
