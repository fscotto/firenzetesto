<?php
/* enqueue script for parent theme stylesheeet */
function childtheme_parent_styles() {

	// enqueue style
	wp_enqueue_style( 'parent', get_template_directory_uri().'/style.css');
	// Register the script like this for a theme:
	wp_enqueue_script( 'map-hover', get_stylesheet_directory_uri() . '/js/map-hover.js', array('jquery'), '1.0.0', true);
}
add_action( 'wp_enqueue_scripts', 'childtheme_parent_styles');

/**********************************************************************************************/

///// AGGIUNTA WIDGET /////
// creo nuovi widget tramite una funzione che potrò richiamare con un hook.
function new_widget() {
	register_sidebar( array( //funzione di wp che crea un widget nel backend.
		'name'          => 'Header 1',
		'id'            => 'header-1',
		'before_widget' => '<div class="header--widget">',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => '',
	) );
	register_sidebar( array(
		'name'          => 'Header 2',
		'id'            => 'header-2',
		'before_widget' => '<div class="header--widget">',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => '',
	) );
	register_sidebar( array(
		'name'          => 'Header 3',
		'id'            => 'header-3',
		'before_widget' => '<div class="header--widget">',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => '',
	) );
	register_sidebar( array(
		'name'          => 'Bottom footer 2',
		'id'            => 'bf-2',
		'before_widget' => '<div class="footer--widget">',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => '',
	) );
	register_sidebar( array(
		'name'          => 'Bottom footer 3',
		'id'            => 'bf-3',
		'before_widget' => '<div class="footer--widget">',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => '',
	) );
}
add_action( 'widgets_init', 'new_widget' );

// Allow SVG uploads
function allow_svgimg_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', 'allow_svgimg_types');

