/**
 * Created by development on 14/01/20.
 */

jQuery(document).ready( function() {
        jQuery(".page-id-21 .header-logo").hide();

jQuery(window).scroll(function() {    
    var scroll = jQuery(window).scrollTop();

     //>=, not <=
    if (scroll >= 1) {
        //clearHeader, not clearheader - caps H
        jQuery("#site-header").addClass("darkHeader");
        jQuery(".page-id-21 .header-logo").show("slow" );
               
    } else {
                jQuery("#site-header").removeClass("darkHeader");

                    jQuery(".page-id-21 .header-logo").hide("slow" );


    }
}); //missing );

    ///// regole per interazione cerchio-rettangolo
    jQuery(function() {
        jQuery('#Ellipse_11').hover(function() {
            jQuery('#box1').css({"background-color": "#EC6A1B"});
            jQuery('#cerchio1').css({"fill": "#EC6A1B"});
            jQuery('.cerchio1bis').css({"display": "block", "fill":"#EC6A1B", "opacity":"0.9"});

        }, function() {
            // on mouseout, reset the background colour
            jQuery('#box1').css('background-color', '');
            jQuery('#cerchio1').css({"fill": ""});
            jQuery('.cerchio1bis').css({"display": "none"});
        });
    });
    jQuery(function() {
        jQuery('#Ellipse_3').hover(function() {
            jQuery('#box3').css({"background-color": "#EC6A1B"});
            jQuery('#cerchio3').css({"fill": "#EC6A1B"});
            jQuery('.cerchio3bis').css({"display": "block", "fill":"#EC6A1B", "opacity":"0.9"});
        }, function() {
            // on mouseout, reset the background colour
            jQuery('#box3').css('background-color', '');
            jQuery('#cerchio3').css({"fill": ""});
            jQuery('.cerchio3bis').css({"display": "none"});
        });
    });

    jQuery(function() {
        jQuery('#Ellipse_16').hover(function() {
            jQuery('#box2').css({"background-color": "#EC6A1B"});
            jQuery('#cerchio2').css({"fill": "#EC6A1B"});
            jQuery('.cerchio2bis').css({"display": "block", "fill":"#EC6A1B", "opacity":"0.9"});
        }, function() {
            // on mouseout, reset the background colour
            jQuery('#box2').css('background-color', '');
            jQuery('#cerchio2').css({"fill": ""});
            jQuery('.cerchio2bis').css({"display": "none"});
        });
    });

    jQuery(function() {
        jQuery('#Ellipse_17').hover(function() {
            jQuery('#box4').css({"background-color": "#EC6A1B"});
            jQuery('#cerchio4').css({"fill": "#EC6A1B"});
            jQuery('.cerchio4bis').css({"display": "block", "fill":"#EC6A1B", "opacity":"0.9"});
        }, function() {
            // on mouseout, reset the background colour
            jQuery('#box4').css('background-color', '');
            jQuery('#cerchio4').css({"fill": ""});
            jQuery('.cerchio4bis').css({"display": "none"});
        });
    });
    ///
    ///// regole per interazione rettangolo-cerchio
    jQuery(function() {
        jQuery('#box1').hover(function() {
            jQuery('#box1').css({"background-color": "#EC6A1B"});
            jQuery('#cerchio1').css('fill', '#EC6A1B');
            jQuery('#Ellipse_11').css('opacity', '0.8');
            jQuery('.cerchio1bis').css({"display": "block", "fill":"#EC6A1B", "opacity":"0.9"});
        }, function() {
            // on mouseout, reset the background colour
            jQuery('#box1').css('background-color', '');
            jQuery('#cerchio1').css('fill', '');
            jQuery('#Ellipse_11').css('opacity', '');
            jQuery('.cerchio1bis').css({"display": "none"});
        });
    });
    jQuery(function() {
        jQuery('#box2').hover(function() {
            jQuery('#box2').css({"background-color": "#EC6A1B"});
            jQuery('#cerchio2').css('fill', '#EC6A1B');
            jQuery('#Ellipse_16').css('opacity', '0.8');
            jQuery('.cerchio2bis').css({"display": "block", "fill":"#EC6A1B", "opacity":"0.9"});
        }, function() {
            // on mouseout, reset the background colour
            jQuery('#box2').css('background-color', '');
            jQuery('#cerchio2').css('fill', '');
            jQuery('#Ellipse_16').css('opacity', '');
            jQuery('.cerchio2bis').css({"display": "none"});
        });
    });
    jQuery(function() {
        jQuery('#box3').hover(function() {
            jQuery('#box3').css({"background-color": "#EC6A1B"});
            jQuery('#cerchio3').css('fill', '#EC6A1B');
            jQuery('#Ellipse_3').css('opacity', '0.8');
            jQuery('.cerchio3bis').css({"display": "block", "fill":"#EC6A1B", "opacity":"0.9"});
        }, function() {
            // on mouseout, reset the background colour
            jQuery('#box3').css('background-color', '');
            jQuery('#cerchio3').css('fill', '');
            jQuery('#Ellipse_3').css('opacity', '');
            jQuery('.cerchio3bis').css({"display": "none"});
        });
    });
    jQuery(function() {
        jQuery('#box4').hover(function() {
            jQuery('#box4').css({"background-color": "#EC6A1B"});
            jQuery('#cerchio4').css('fill', '#EC6A1B');
            jQuery('#Ellipse_17').css('opacity', '0.8');
            jQuery('.cerchio4bis').css({"display": "block", "fill":"#EC6A1B", "opacity":"0.9"});
        }, function() {
            // on mouseout, reset the background colour
            jQuery('#box4').css('background-color', '');
            jQuery('#cerchio4').css('fill', '');
            jQuery('#Ellipse_17').css('opacity', '');
            jQuery('.cerchio4bis').css({"display": "none"});
        });
    });
    ///

});
