<?php
/*
Template Name: espositori
*/
get_header();
?>

<main id="site-content" role="main">


<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">


	<div class="post-inner <?php echo is_page_template( 'templates/template-full-width.php' ) ? '' : 'thin'; ?> ">

		<div class="entry-content">

			<?php 
				the_content( __( 'Continue reading', 'twentytwenty' ) );

$output = '[vc_row][vc_column][vc_column_text]

[/vc_column_text][/vc_column][/vc_row][vc_section css_animation="fadeInDown" css=".vc_custom_1579883928858{background-position: 0 0 !important;background-repeat: repeat !important;}" el_class="sfondo-quadrati"][vc_row el_class="main-content"][vc_column][vc_row_inner css=".vc_custom_1579173861654{background-image: url(https://firenzetesto.com/wp-content/uploads/2020/01/vertical-dot-black.png?id=432) !important;}" el_class="vertical-dot"][vc_column_inner][/vc_column_inner][/vc_row_inner][vc_row_inner][vc_column_inner width="1/2"][vc_column_text el_class="pag-int-title"]
<h1>Espositori</h1>
[/vc_column_text][/vc_column_inner][vc_column_inner width="1/2"][vc_column_text el_class="pag-int-p" el_id="comunicazione-paragrafo"]Lorem ipsum lorem ipsum.
Lorem ipsum[/vc_column_text][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row][vc_row full_width="stretch_row_content" content_placement="bottom" el_class="scroll-down"][vc_column][vc_empty_space height="80px"][/vc_column][/vc_row][vc_row full_width="stretch_row_content" content_placement="bottom" el_class="scroll-down" css=".vc_custom_1579180844744{background-image: url(https://firenzetesto.com/wp-content/uploads/2020/01/pattern-dotted-black.png?id=423) !important;}"][vc_column][vc_empty_space height="80px"][/vc_column][/vc_row][vc_row full_width="stretch_row_content" content_placement="bottom" el_class="sfondo-quadrati" css=".vc_custom_1579182315120{background-image: url(https://firenzetesto.com/wp-content/uploads/2020/01/repeat-pattern-2.png?id=483) !important;background-position: 0 0 !important;background-repeat: repeat !important;}"][vc_column][vc_empty_space height="80px"][/vc_column][/vc_row][/vc_section]
';

echo do_shortcode($output); ?>


	<?php 
	//new loop

	// Set the arguments for the query
	$args = array( 
	  'numberposts'		=> -1, // -1 is for all
	  'post_type'		=> 'espositori', // or 'post', 'page'
	  'orderby' 		=> 'title', // or 'date', 'rand'
	  'order' 		=> 'ASC', // or 'DESC'
	);
	// Get the posts
	$myposts = get_posts($args);

	// If there are posts
	if($myposts):
	  // Loop the posts
	  foreach ($myposts as $mypost):
	?>
	  <div class="row">
	    <!-- Image -->
	    <div class="col-xs-3"> 
	    </div> 	
	    
	    <!-- Content -->
	    <div class="col-xs-9">
	      <h1><a href="<?php echo get_permalink($mypost->ID); ?>"><?php echo get_the_title($mypost->ID); ?></a></h1>
	      <?php echo $trimmed = wp_trim_words(get_the_content($mypost->ID), 15, '...'); ?>
	    </div>
	  </div>

	  <?php endforeach; wp_reset_postdata(); ?>
	<?php endif; ?>

		</div><!-- .entry-content -->

	</div><!-- .post-inner -->


</article><!-- .post -->




</main><!-- #site-content -->

<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>

<?php get_footer(); ?>

